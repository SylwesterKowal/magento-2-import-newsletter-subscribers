<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 26/06/2020
 * Time: 11:00
 */

namespace Kowal\ImportNewsletterSubscribers\Lib;

use Magento\Framework\App\State;
use Magento\Store\Model\StoreManager;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ObjectManagerFactory;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Newsletter\Model\Subscriber;

class Import
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $_directoryList;

    /**
     * @var Magento\Newsletter\Model\SubscriberFactory
     */
    protected $_subscriberFactory;

    /**
     *
     */
    const CSV_EMAIL_POSITION = 0;

    /**
     * Import constructor.
     * @param DirectoryList $_directoryList
     * @param SubscriberFactory $_subscriberFactory
     */
    public function __construct(
//        ObjectManagerFactory $objectManagerFactory,
        DirectoryList $_directoryList,
        SubscriberFactory $_subscriberFactory
    )
    {
//        $params = $_SERVER;
//
//        $params[StoreManager::PARAM_RUN_CODE] = 'admin';
//        $params[StoreManager::PARAM_RUN_TYPE] = 'store';
//        $this->_objectManager = $objectManagerFactory->create($params);


        $this->_directoryList = $_directoryList;
        $this->_subscriberFactory = $_subscriberFactory;
    }

    /**
     * @param $csvPath
     * @return bool
     */
    public function execute($csvPath)
    {


        if (!file_exists($csvPath)) {
            throw new \Exception(__("<error>CVS file not found in {$csvPath}</error>"));
        }

        $csv = array_map('str_getcsv', file($csvPath));
        $count = count($csv);

        foreach ($csv as $key => $row) {
            $email = $row[self::CSV_EMAIL_POSITION];
            if (!$this->isValidEmail($email)) {
                continue 1;
            }

            $this->_subscriberFactory->create()
                ->setStatus(Subscriber::STATUS_SUBSCRIBED)
                ->setEmail($email)
                ->save();
        }
        return true;
    }

    /**
     * Remove all illegal characters from email and validates it.
     *
     * @param string $email
     * @return bool
     */
    protected function isValidEmail(string $email)
    {
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);

        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return false;
        }

        return true;
    }
}